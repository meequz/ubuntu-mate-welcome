#
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: : LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: gettingstarted.html:18
msgid "Getting Started"
msgstr ""

#: gettingstarted.html:27
msgid "Prerequisites"
msgstr ""

#: gettingstarted.html28, 28, 159, 160
msgid "System Requirements"
msgstr ""

#: gettingstarted.html29, 90, 91, 337
msgid "Installation Preparations"
msgstr ""

#: gettingstarted.html:29
msgid "Installation Preparation"
msgstr ""

#: gettingstarted.html30, 30, 161, 162, 172
msgid "Dual Booting"
msgstr ""

#: gettingstarted.html:31
msgid "Install Now"
msgstr ""

#: gettingstarted.html:35
msgid "Post-Installation"
msgstr ""

#: gettingstarted.html36, 36, 348, 569, 570
msgid "Software Updates"
msgstr ""

#: gettingstarted.html37, 37, 384, 385, 395, 615, 616
msgid "Drivers"
msgstr ""

#: gettingstarted.html38, 38
msgid "Language & Input"
msgstr ""

#: gettingstarted.html39, 39, 617, 618, 757, 758
msgid "Optional Tasks"
msgstr ""

#: gettingstarted.html:41
msgid "Familiarity"
msgstr ""

#: gettingstarted.html42, 42, 710, 711, 849, 850
msgid "Customization"
msgstr ""

#: gettingstarted.html43, 43, 759, 760, 770
msgid "Keyboard Shortcuts"
msgstr ""

#: gettingstarted.html:46
msgid "Troubleshooting"
msgstr ""

#: gettingstarted.html47, 47, 85
msgid "System Specifications"
msgstr ""

#: gettingstarted.html:55
msgid "Please choose a topic to get started."
msgstr ""

#: gettingstarted.html:63
msgid "Modest System Requirements"
msgstr ""

#: gettingstarted.html:64
msgid ""
"Ubuntu MATE works well on both high-end to slow computers, optimized to be "
"functional, while light on system resources."
msgstr ""

#: gettingstarted.html:67
msgid "Minimum"
msgstr ""

#: gettingstarted.html:69
msgid "Pentium III 750 megahertz (MHz)"
msgstr ""

#: gettingstarted.html:70
msgid "512 megabytes (MB) of RAM"
msgstr ""

#: gettingstarted.html:71
msgid "8 gigabytes (GB) of disk space"
msgstr ""

#: gettingstarted.html:74
msgid "Recommended"
msgstr ""

#: gettingstarted.html:76
msgid "Core 2 Duo 1.6 gigahertz (GHz)"
msgstr ""

#: gettingstarted.html:77
msgid "2 gigabytes (GB) of RAM"
msgstr ""

#: gettingstarted.html:78
msgid "16 gigabytes (GB) of disk space"
msgstr ""

#: gettingstarted.html:81
msgid ""
"The operating system will run much more efficiently if your computer meets "
"or surpasses the recommended requirements."
msgstr ""

#: gettingstarted.html85, 860
msgid "What's inside my computer?"
msgstr ""

#: gettingstarted.html91, 162, 385, 572, 618, 711, 760
msgid "Next:"
msgstr ""

#: gettingstarted.html:101
msgid "Preparing to Install"
msgstr ""

#: gettingstarted.html:102
msgid ""
"Ubuntu MATE is relatively simple to set up, whether you're looking to "
"replace your existing operating system or install Ubuntu MATE alongside an "
"existing one - such as"
msgstr ""

#: gettingstarted.html:105
msgid "Microsoft Windows or another"
msgstr ""

#: gettingstarted.html:106
msgid "GNU/Linux distribution."
msgstr ""

#: gettingstarted.html:108
msgid "For the best results:"
msgstr ""

#: gettingstarted.html110, 185
msgid "Back up"
msgstr ""

#: gettingstarted.html:110
msgid "any important data to another storage device."
msgstr ""

#: gettingstarted.html:111
msgid ""
"Connect to the Internet to install all available updates during "
"installation."
msgstr ""

#: gettingstarted.html:112
msgid "Stay connected to the Internet to install all available updates."
msgstr ""

#: gettingstarted.html:113
msgid "Plug in your device to the mains if it is running on battery power."
msgstr ""

#: gettingstarted.html:118
msgid ""
"The live session you are currently in right now is a good opportunity to "
"test Ubuntu MATE without modifying your computer, allowing you to test all "
"of your peripherals are working as intended before committing to installing "
"it on your machine. Once you are ready, double click the installation icon "
"from the desktop."
msgstr ""

#: gettingstarted.html:125
msgid "Modern UEFI-based computers"
msgstr ""

#: gettingstarted.html:126
msgid ""
"If you have a modern PC that uses the Unifed Extended Firmware Interface "
"(UEFI), you may need to disable"
msgstr ""

#: gettingstarted.html:127
msgid "Secure Boot"
msgstr ""

#: gettingstarted.html:127
msgid "(or enable an"
msgstr ""

#: gettingstarted.html:127
msgid "Unlock Bootloader"
msgstr ""

#: gettingstarted.html:128
msgid ""
"option) prior to installation, particularly if an operating system was pre-"
"installed."
msgstr ""

#: gettingstarted.html:130
msgid "If you are using a legacy BIOS machine, you do not need to do this."
msgstr ""

#: gettingstarted.html:133
msgid ""
"Especially when dual booting, it is important that you boot your system in "
"the correct mode and use the"
msgstr ""

#: gettingstarted.html:134
msgid "64-bit image"
msgstr ""

#: gettingstarted.html:134
msgid ""
", as you cannot boot an operating system installed in BIOS mode after "
"installing a UEFI-enabled operating system or vice versa."
msgstr ""

#: gettingstarted.html:139
msgid ""
"Booting in BIOS mode on a UEFI system with a GPT-formatted disk will also "
"show a \"protection layer\"."
msgstr ""

#: gettingstarted.html:140
msgid "Do not worry,"
msgstr ""

#: gettingstarted.html:140
msgid "your data is not lost or corrupt!"
msgstr ""

#: gettingstarted.html:144
msgid "Your system is currently running in"
msgstr ""

#: gettingstarted.html:144
msgid "mode"
msgstr ""

#: gettingstarted.html:149
msgid "Swap"
msgstr ""

#: gettingstarted.html:150
msgid ""
"For computers that start to run low on memory (RAM), the operating system "
"will start swapping to disk. Known as a \"paging file\" in Windows. "
"Performance will decrease when running low on memory and could possibly "
"freeze if you completely run out of memory without anywhere to swap."
msgstr ""

#: gettingstarted.html:154
msgid "By default,"
msgstr ""

#: gettingstarted.html:154
msgid "a swap partition is created by the size of your RAM."
msgstr ""

#: gettingstarted.html:154
msgid ""
"For example, if you have 2 GiB of RAM, then 2 GiB of hard disk space will be"
" reserved for swap at the end of the drive."
msgstr ""

#: gettingstarted.html160, 338, 570, 616, 709, 758, 850
msgid "Previous:"
msgstr ""

#: gettingstarted.html:173
msgid ""
"Should you wish to install Ubuntu MATE alongside your existing operating "
"system, your partitions need to be shrunk. This results in sections of your "
"hard disk being split between multiple operating systems."
msgstr ""

#: gettingstarted.html:182
msgid "To minimize the risk of data loss or failure, ensure that:"
msgstr ""

#: gettingstarted.html:184
msgid "There is enough free space. 10 GB minimum is recommended."
msgstr ""

#: gettingstarted.html:185
msgid "important data to another drive."
msgstr ""

#: gettingstarted.html:186
msgid "Defragment the hard drive."
msgstr ""

#: gettingstarted.html:187
msgid "Ensure Windows has been cleanly shut down (not hibernated)."
msgstr ""

#: gettingstarted.html:191
msgid "Which approach would you prefer?"
msgstr ""

#: gettingstarted.html193, 277
msgid "Simple"
msgstr ""

#: gettingstarted.html194, 277
msgid "Advanced"
msgstr ""

#: gettingstarted.html:195
msgid "Safest"
msgstr ""

#: gettingstarted.html:200
msgid "The simplest method is to use the installer."
msgstr ""

#: gettingstarted.html:202
msgid ""
"The installer scans your disks for existing operating systems and provides a"
" choice on what you'd like to do."
msgstr ""

#: gettingstarted.html:205
msgid "You will see an option similar to this:"
msgstr ""

#: gettingstarted.html:209
msgid "Followed by:"
msgstr ""

#: gettingstarted.html:215
msgid "Do not interrupt or power off the computer during a resize operation."
msgstr ""

#: gettingstarted.html:217
msgid ""
"If you do, data loss or a partition table corruption could occur. Please "
"double check your changes before proceeding."
msgstr ""

#: gettingstarted.html:219
msgid "The installer may show a"
msgstr ""

#: gettingstarted.html:219
msgid ""
"spinner cursor for a long period of time, depending on the size of your disk"
msgstr ""

#: gettingstarted.html:223
msgid "When you next boot Windows, run"
msgstr ""

#: gettingstarted.html:224
msgid ""
"from the command prompt to check the file system for errors. Windows may "
"schedule this automatically. This ensures consistency for the NTFS file "
"system."
msgstr ""

#: gettingstarted.html:231
msgid "Use GParted or the installer's \"Custom\" option."
msgstr ""

#: gettingstarted.html234, 988
msgid "GParted"
msgstr ""

#: gettingstarted.html:234
msgid "is available during the live session only."
msgstr ""

#: gettingstarted.html:235
msgid "It is located under the"
msgstr ""

#: gettingstarted.html235, 362, 415, 535, 591, 650, 675, 700, 731, 744, 880
msgid "System"
msgstr ""

#: gettingstarted.html236, 362, 415, 536, 700
msgid "Administration"
msgstr ""

#: gettingstarted.html:236
msgid ""
"menu. GParted provides complete flexibility over your partitions, primarily "
"for power users who know how they'd like to layout their disks. This method "
"makes changes to partitions prior to installation."
msgstr ""

#: gettingstarted.html:244
msgid ""
"After completing operations to your disk, you can leave the remaining space "
"unallocated,"
msgstr ""

#: gettingstarted.html:245
msgid ""
"whereby the installer will detect this unused space to install Ubuntu MATE "
"to."
msgstr ""

#: gettingstarted.html:247
msgid "Using the Installer's \"Custom\" option."
msgstr ""

#: gettingstarted.html:248
msgid "Instead or in addition to GParted, the"
msgstr ""

#: gettingstarted.html:248
msgid "Custom"
msgstr ""

#: gettingstarted.html:248
msgid ""
"option during the installation wizard can also configure and resize your "
"partitions as well as choose mount points. Dividing your disks can make it "
"somewhat easier to re-install the operating system later if desired."
msgstr ""

#: gettingstarted.html:253
msgid "You can specify these mount points:"
msgstr ""

#: gettingstarted.html:255
msgid "The base for Ubuntu MATE"
msgstr ""

#: gettingstarted.html:256
msgid "Bootloader and kernels"
msgstr ""

#: gettingstarted.html:257
msgid "Your personal files and folders"
msgstr ""

#: gettingstarted.html:258
msgid "Temporary files"
msgstr ""

#: gettingstarted.html:259
msgid "Contains most of the applications"
msgstr ""

#: gettingstarted.html:259
msgid "(binaries, documentation, libraries, etc)"
msgstr ""

#: gettingstarted.html:260
msgid "Variable data"
msgstr ""

#: gettingstarted.html:260
msgid "(such as system logs)"
msgstr ""

#: gettingstarted.html:261
msgid "Data for System Services"
msgstr ""

#: gettingstarted.html:261
msgid "(typically used for server configurations)"
msgstr ""

#: gettingstarted.html:262
msgid "Additional software unmanaged by the package manager"
msgstr ""

#: gettingstarted.html:263
msgid "such as"
msgstr ""

#: gettingstarted.html:265
msgid ""
"Creating separate partitions for all above mount points is not necessary."
msgstr ""

#: gettingstarted.html:266
msgid "The commonly partitioned directories being"
msgstr ""

#: gettingstarted.html:267
msgid "and"
msgstr ""

#: gettingstarted.html:267
msgid "These mount points can also be set across different"
msgstr ""

#: gettingstarted.html:268
msgid "physical disks, for instance, if you'd like the OS"
msgstr ""

#: gettingstarted.html:268
msgid "on an SSD,"
msgstr ""

#: gettingstarted.html:269
msgid "but your personal files"
msgstr ""

#: gettingstarted.html:269
msgid "on a mechanical hard drive."
msgstr ""

#: gettingstarted.html:273
msgid "Shrink your system disk within Windows."
msgstr ""

#: gettingstarted.html:275
msgid "Applies to Windows Vista and later."
msgstr ""

#: gettingstarted.html:276
msgid "If you are using Windows XP or earlier, you will need to use the"
msgstr ""

#: gettingstarted.html:277
msgid "or"
msgstr ""

#: gettingstarted.html:277
msgid "method to shrink your disk."
msgstr ""

#: gettingstarted.html:280
msgid ""
"Newer versions of Windows provide a tool to shrink the NTFS volume from "
"within Windows. This is a safer bet if it's essential that Windows still "
"boots on your computer while experiencing Ubuntu MATE."
msgstr ""

#: gettingstarted.html284, 560, 959, 976, 992, 1008
msgid "Open"
msgstr ""

#: gettingstarted.html:284
msgid "Disk Management"
msgstr ""

#: gettingstarted.html:284
msgid "from the start menu."
msgstr ""

#: gettingstarted.html:289
msgid "Right click your desired drive and choose"
msgstr ""

#: gettingstarted.html:289
msgid "Shrink Partition"
msgstr ""

#: gettingstarted.html:289
msgid "from the context menu."
msgstr ""

#: gettingstarted.html:294
msgid "Enter the amount to shrink your system partition by."
msgstr ""

#: gettingstarted.html:300
msgid "Remember, Ubuntu MATE needs at minimum 8 GB of disk space."
msgstr ""

#: gettingstarted.html:305
msgid "Further Reading"
msgstr ""

#: gettingstarted.html:306
msgid ""
"For further information on the best practices to modify partitions "
"containing Windows, see the following"
msgstr ""

#: gettingstarted.html:309
msgid "help article"
msgstr ""

#: gettingstarted.html:309
msgid "online"
msgstr ""

#: gettingstarted.html:313
msgid "Repairing Boot Problems"
msgstr ""

#: gettingstarted.html:315
msgid "Linux distributions use a boot loader known as"
msgstr ""

#: gettingstarted.html:316
msgid ""
"which looks for and bootstraps the operating system. However, some operating"
" systems (such as Windows) do not integrate with existing boot loaders and "
"will overwrite them, with the potential to lose access to Ubuntu MATE "
"temporally."
msgstr ""

#: gettingstarted.html:321
msgid "Fortunately, you can use the"
msgstr ""

#: gettingstarted.html:321
msgid "Boot Repair"
msgstr ""

#: gettingstarted.html:321
msgid "tool to repair common problems caused by other operating systems."
msgstr ""

#: gettingstarted.html:326
msgid "Requires downloading a package."
msgstr ""

#: gettingstarted.html:327
msgid "Please connect to the Internet to download and use this utility."
msgstr ""

#: gettingstarted.html:328
msgid "Sorry, Welcome was unable to establish a connection."
msgstr ""

#: gettingstarted.html:329
msgid "Retry"
msgstr ""

#: gettingstarted.html:332
msgid "Download Boot Repair"
msgstr ""

#: gettingstarted.html:333
msgid "Open Boot-Repair"
msgstr ""

#: gettingstarted.html:338
msgid "Install Preparations"
msgstr ""

#: gettingstarted.html:351
msgid "An Internet connection is required to download and install updates."
msgstr ""

#: gettingstarted.html:353
msgid ""
"Ubuntu MATE periodically will check to see if new software and security "
"updates are available for your computer. If you have just installed Ubuntu "
"MATE, you might want to do this now."
msgstr ""

#: gettingstarted.html358, 361
msgid "Software Updater"
msgstr ""

#: gettingstarted.html361, 414, 590, 649, 674, 699, 730
msgid "can be found later in"
msgstr ""

#: gettingstarted.html:366
msgid ""
"The codecs package include a complete, cross-platform solution to decode, "
"encode, record, convert and stream audio and video. It also includes a MP3 "
"audio decoder that permits the playback of MPEG 1 audio layer III (MP3)."
msgstr ""

#: gettingstarted.html:370
msgid ""
"You'll need the Blu-ray AACS database in order to play AACS encrypted or BD+"
" protected discs."
msgstr ""

#: gettingstarted.html:374
msgid "Install Codecs Package"
msgstr ""

#: gettingstarted.html:377
msgid "Install Blu-ray AACS Database"
msgstr ""

#: gettingstarted.html:379
msgid "Installing..."
msgstr ""

#: gettingstarted.html:396
msgid ""
"While a wide range of hardware is compatible with Ubuntu MATE, there are "
"some components and peripherals that require proprietary drivers to function"
" properly. Installing the firmware package is often required to get some "
"devices, typically Bluetooth and Wi-Fi, to work correctly."
msgstr ""

#: gettingstarted.html:402
msgid "An Internet connection is required to download and install drivers."
msgstr ""

#: gettingstarted.html406, 414, 484
msgid "Additional Drivers"
msgstr ""

#: gettingstarted.html:409
msgid "Install Firmware Package"
msgstr ""

#: gettingstarted.html:423
msgid ""
"Sorry, Welcome was unable to automatically identify the graphics vendor on "
"this system."
msgstr ""

#: gettingstarted.html424, 430, 438, 444
msgid "Card/Chipset:"
msgstr ""

#: gettingstarted.html424, 430, 438, 444
msgid "Unknown"
msgstr ""

#: gettingstarted.html428, 434
msgid "Graphics Card Detected."
msgstr ""

#: gettingstarted.html:429
msgid ""
"may have drivers for your card that can boost performance for 3D "
"applications and games as well as improved power management."
msgstr ""

#: gettingstarted.html:435
msgid ""
"AMD's drivers are no longer supported on Ubuntu. The open source drivers you"
" are currently using will deliver the most stable performance out of your "
"card."
msgstr ""

#: gettingstarted.html:442
msgid "You're already good to go."
msgstr ""

#: gettingstarted.html:443
msgid ""
"Intel's drivers are open source and are maintained/updated in the kernel."
msgstr ""

#: gettingstarted.html:448
msgid "VirtualBox Guest Additions"
msgstr ""

#: gettingstarted.html:449
msgid ""
"To accelerate graphics performance inside the virtual machine, please "
"install Guest Additions."
msgstr ""

#: gettingstarted.html:453
msgid "Proprietary or Open Source Drivers?"
msgstr ""

#: gettingstarted.html:454
msgid "Proprietary drivers come directly from the manufacturer."
msgstr ""

#: gettingstarted.html:455
msgid "As they are not open source, Ubuntu developers are"
msgstr ""

#: gettingstarted.html:455
msgid "unable review and improve code"
msgstr ""

#: gettingstarted.html:455
msgid "meaning you'll have to rely on the manufacturer for support."
msgstr ""

#: gettingstarted.html:458
msgid "In addition, proprietary drivers can"
msgstr ""

#: gettingstarted.html:458
msgid "significantly improve graphics performance"
msgstr ""

#: gettingstarted.html:458
msgid "and as a result,"
msgstr ""

#: gettingstarted.html:459
msgid "the entire system. Users may wish to install these drivers to:"
msgstr ""

#: gettingstarted.html:461
msgid "Run 3D applications and games."
msgstr ""

#: gettingstarted.html:462
msgid "Use Compiz's window effects."
msgstr ""

#: gettingstarted.html:463
msgid ""
"Improve power management, especially if the device overheats then usual "
"under open source drivers."
msgstr ""

#: gettingstarted.html:464
msgid "Improve support for high resolutions and multiple monitors."
msgstr ""

#: gettingstarted.html:465
msgid "Fix blank screen issues."
msgstr ""

#: gettingstarted.html:468
msgid ""
"Proprietary drivers are known as \"binary blobs\". Certain hardware may "
"experience worse behavior,"
msgstr ""

#: gettingstarted.html:469
msgid "causing more issues then the open source drivers, such as:"
msgstr ""

#: gettingstarted.html:471
msgid "Failing to boot to the graphical desktop."
msgstr ""

#: gettingstarted.html:472
msgid "Broken drivers after an upgrade."
msgstr ""

#: gettingstarted.html:473
msgid "Severe graphical glitches."
msgstr ""

#: gettingstarted.html:474
msgid "Problems with applications/games requiring hardware acceleration."
msgstr ""

#: gettingstarted.html:478
msgid "is the open source driver for your card."
msgstr ""

#: gettingstarted.html:483
msgid "What about the latest NVIDIA drivers?"
msgstr ""

#: gettingstarted.html:484
msgid "fetches the latest tested driver from the Ubuntu repositories."
msgstr ""

#: gettingstarted.html:485
msgid ""
"Advanced users and gamers who would like newer versions may wish to consider"
" adding the"
msgstr ""

#: gettingstarted.html:486
msgid "Ubuntu Graphics Driver PPA"
msgstr ""

#: gettingstarted.html:487
msgid ""
"which packages up the latest NVIDIA drivers for Ubuntu. These packages are "
"not as thoroughly tested."
msgstr ""

#: gettingstarted.html:489
msgid ""
"This only applies for actively supported cards, and not older cards using "
"the legacy driver."
msgstr ""

#: gettingstarted.html:492
msgid "Add Repository"
msgstr ""

#: gettingstarted.html:495
msgid ""
"To add the repository to the system, open a terminal and type the following:"
msgstr ""

#: gettingstarted.html:500
msgid "After the PPA is added, newer versions are presented to install in"
msgstr ""

#: gettingstarted.html:501
msgid "Additional Drivers:"
msgstr ""

#: gettingstarted.html:508
msgid "Wireless, Bluetooth and Other Devices"
msgstr ""

#: gettingstarted.html:510
msgid ""
"If there were no additional drivers found for the hardware nor did it work "
"after installing the firmware packages, you may need to research your device"
" to find the appropriate driver."
msgstr ""

#: gettingstarted.html:514
msgid ""
"Unfortunately, this is due to the fact the manufacturer for your hardware "
"hasn't provided any source code for developers to freely use, or there is "
"insufficient documentation to create drivers for your particular brand and "
"model."
msgstr ""

#: gettingstarted.html:520
msgid ""
"Some manufacturers may provide closed source drivers provided by themselves "
"directly. If you require assistance, feel free to"
msgstr ""

#: gettingstarted.html:522
msgid "ask the community"
msgstr ""

#: gettingstarted.html:525
msgid "Read more on troubleshooting wireless hardware."
msgstr ""

#: gettingstarted.html529, 534
msgid "Printers"
msgstr ""

#: gettingstarted.html:531
msgid "When you set up a printer in the"
msgstr ""

#: gettingstarted.html:535
msgid "utility"
msgstr ""

#: gettingstarted.html:535
msgid "under"
msgstr ""

#: gettingstarted.html:536
msgid ""
"you will be presented with a list of drivers available to use, if "
"applicable."
msgstr ""

#: gettingstarted.html:539
msgid ""
"Some manufacturers may provide their own drivers and require you to download"
" them from their website."
msgstr ""

#: gettingstarted.html:542
msgid "Read more on printing"
msgstr ""

#: gettingstarted.html:553
msgid "HP Linux Printing and Imaging System"
msgstr ""

#: gettingstarted.html:554
msgid ""
"Recommended if you have an HP printer as it provides full support for "
"printing on most HP SFP (single function peripheral) inkjets and many "
"LaserJets, and for scanning, sending faxes and for photo-card access on most"
" HP MFP (multi-function peripheral) printers."
msgstr ""

#: gettingstarted.html561, 960, 977, 993, 1009
msgid "Install"
msgstr ""

#: gettingstarted.html571, 572, 708, 709
msgid "Language Input"
msgstr ""

#: gettingstarted.html582, 587, 590
msgid "Language Support"
msgstr ""

#: gettingstarted.html:583
msgid ""
"You computer may require updates to your language and localization settings,"
" or you may want to install additional language support."
msgstr ""

#: gettingstarted.html591, 650, 675, 731
msgid "Preferences"
msgstr ""

#: gettingstarted.html592, 651
msgid "Personal"
msgstr ""

#: gettingstarted.html:598
msgid "Complex Input"
msgstr ""

#: gettingstarted.html:599
msgid "Some regions of the world require complex inputs."
msgstr ""

#: gettingstarted.html:604
msgid "Chinese"
msgstr ""

#: gettingstarted.html:607
msgid "Japanese"
msgstr ""

#: gettingstarted.html:610
msgid "Korean"
msgstr ""

#: gettingstarted.html:626
msgid "Backup, Firewall and User Management"
msgstr ""

#: gettingstarted.html:627
msgid ""
"Ubuntu MATE provides tools to backup your files, comes with a firewall pre-"
"installed and supports multiple user accounts."
msgstr ""

#: gettingstarted.html638, 649
msgid "Backups"
msgstr ""

#: gettingstarted.html:639
msgid ""
"Nothing is more important then having a backup of your personal files, just "
"in case something disastrous happens to your computer."
msgstr ""

#: gettingstarted.html:643
msgid ""
"comes pre-installed and supports backups to local devices as well as online "
"cloud-based services."
msgstr ""

#: gettingstarted.html:646
msgid "Configure Backup"
msgstr ""

#: gettingstarted.html:662
msgid "Firewall"
msgstr ""

#: gettingstarted.html:663
msgid ""
"Firewalls prevent malicious connections entering and leaving your computer. "
"By default, the firewall is disabled,"
msgstr ""

#: gettingstarted.html:665
msgid "learn more about firewalls"
msgstr ""

#: gettingstarted.html:667
msgid "provides a graphical frontend to"
msgstr ""

#: gettingstarted.html:668
msgid "Uncomplicated Firewall"
msgstr ""

#: gettingstarted.html:668
msgid "If security is important to you, there are"
msgstr ""

#: gettingstarted.html:669
msgid "more ways to stay secure too"
msgstr ""

#: gettingstarted.html:671
msgid "Configure Firewall"
msgstr ""

#: gettingstarted.html:674
msgid "Firewall Configuration"
msgstr ""

#: gettingstarted.html:676
msgid "Internet and Network"
msgstr ""

#: gettingstarted.html:687
msgid "Users"
msgstr ""

#: gettingstarted.html:689
msgid ""
"Share your computer with others? Set up each user with their own unique "
"profile that they can use to login and isolate their personal data with "
"yours."
msgstr ""

#: gettingstarted.html:693
msgid "You can also log in to a"
msgstr ""

#: gettingstarted.html:693
msgid "Guest Session"
msgstr ""

#: gettingstarted.html:693
msgid "if someone is using your computer as a one-off."
msgstr ""

#: gettingstarted.html:696
msgid "Configure Users"
msgstr ""

#: gettingstarted.html699, 701
msgid "Users and Groups"
msgstr ""

#: gettingstarted.html719, 727
msgid "User Interface"
msgstr ""

#: gettingstarted.html:720
msgid ""
"Ubuntu MATE provides a unique tool to transform the user interface. It can "
"be used to quickly setup the user interface to broadly reflect how other "
"popular operating systems look and feel. The"
msgstr ""

#: gettingstarted.html:723
msgid "layouts are most similar to Microsoft Windows while"
msgstr ""

#: gettingstarted.html:724
msgid "layouts are most similar to Apple Mac OS X."
msgstr ""

#: gettingstarted.html:730
msgid "MATE Tweak"
msgstr ""

#: gettingstarted.html:732
msgid "Look and Feel"
msgstr ""

#: gettingstarted.html735, 740, 743
msgid "Control Center"
msgstr ""

#: gettingstarted.html:736
msgid ""
"The utilities mentioned above are all part of the extensive Control Center "
"that Ubuntu MATE provides. You can configure just about every aspect of your"
" computer from the Control Center."
msgstr ""

#: gettingstarted.html:743
msgid "can be found later in the"
msgstr ""

#: gettingstarted.html:744
msgid "menu."
msgstr ""

#: gettingstarted.html:747
msgid "Desktop Wallpaper"
msgstr ""

#: gettingstarted.html:748
msgid ""
"The Ubuntu MATE Community have contributed dozens of desktop wallpaper "
"images over the years. Why not install them and find something that suits "
"your tastes?"
msgstr ""

#: gettingstarted.html:752
msgid "Wallpapers"
msgstr ""

#: gettingstarted.html:771
msgid ""
"Ubuntu MATE has many keyboard shortcuts that help make using your computer "
"more efficient."
msgstr ""

#: gettingstarted.html:773
msgid "Common application shortcuts"
msgstr ""

#: gettingstarted.html:774
msgid "These shortcuts apply in most applications."
msgstr ""

#: gettingstarted.html:776
msgid "Copy the selected text/object"
msgstr ""

#: gettingstarted.html:777
msgid "Cut the selected text/object"
msgstr ""

#: gettingstarted.html:778
msgid "Paste/insert the selected text/object"
msgstr ""

#: gettingstarted.html:779
msgid "Select all text"
msgstr ""

#: gettingstarted.html:780
msgid "Find and replace words"
msgstr ""

#: gettingstarted.html:781
msgid "Make the selected text bold"
msgstr ""

#: gettingstarted.html:782
msgid "Make the selected text italic"
msgstr ""

#: gettingstarted.html:783
msgid "Underline the selected text"
msgstr ""

#: gettingstarted.html:784
msgid "Open a new document or window"
msgstr ""

#: gettingstarted.html:785
msgid "Save the current document"
msgstr ""

#: gettingstarted.html:786
msgid "Open another document"
msgstr ""

#: gettingstarted.html:787
msgid "Print the current document"
msgstr ""

#: gettingstarted.html:788
msgid "Undo the last change you made"
msgstr ""

#: gettingstarted.html:789
msgid "Redo a change that you just undid"
msgstr ""

#: gettingstarted.html:790
msgid "Toggle the current application between full-screen and windowed"
msgstr ""

#: gettingstarted.html:792
msgid "Desktop shortcuts"
msgstr ""

#: gettingstarted.html:793
msgid ""
"This section lists common keyboard shortcuts which you can use to operate "
"parts of the desktop."
msgstr ""

#: gettingstarted.html:796
msgid "Open the Applications menu"
msgstr ""

#: gettingstarted.html:797
msgid "Run an application by typing its name in the box which appears"
msgstr ""

#: gettingstarted.html:798
msgid "Prt Sc"
msgstr ""

#: gettingstarted.html:798
msgid "Take a screenshot of the whole screen"
msgstr ""

#: gettingstarted.html:799
msgid "Take a screenshot of the current window"
msgstr ""

#: gettingstarted.html:800
msgid "Delete"
msgstr ""

#: gettingstarted.html:800
msgid "Prompt is you want to Suspend, Restart or Shutdown the computer"
msgstr ""

#: gettingstarted.html:801
msgid "Lock your screen."
msgstr ""

#: gettingstarted.html:802
msgid "Open a terminal."
msgstr ""

#: gettingstarted.html:804
msgid "Window shortcuts"
msgstr ""

#: gettingstarted.html:806
msgid "Tab"
msgstr ""

#: gettingstarted.html:806
msgid ""
"Switch between currently-open windows. Press Alt + Tab and then release Tab "
"(but continue to hold Alt). Press Tab repeatedly to cycle through the list "
"of available windows which appears on the screen. Release the Alt key to "
"switch to the selected window."
msgstr ""

#: gettingstarted.html:807
msgid ""
"Switch between currently-open windows in all Workspaces. Press Tab "
"repeatedly to cycle through the list of available windows which appears on "
"the screen. Release the Ctrl and Alt keys to switch to the selected window."
msgstr ""

#: gettingstarted.html:808
msgid "Left"
msgstr ""

#: gettingstarted.html:808
msgid "Switch to the next workspace."
msgstr ""

#: gettingstarted.html:809
msgid "Right"
msgstr ""

#: gettingstarted.html:809
msgid "Switch to the previous workspaces."
msgstr ""

#: gettingstarted.html810, 811, 837
msgid "Shift"
msgstr ""

#: gettingstarted.html:810
msgid "Move the current window to the next workspace."
msgstr ""

#: gettingstarted.html:811
msgid "Move the current window to the previous workspace."
msgstr ""

#: gettingstarted.html:812
msgid "Closes window."
msgstr ""

#: gettingstarted.html:813
msgid "Returns window to 'normal' or previous size."
msgstr ""

#: gettingstarted.html:814
msgid "Moves the current window (can be moved with mouse or keyboard)."
msgstr ""

#: gettingstarted.html:815
msgid "Resizes current window (again, can be moved with mouse or keyboard)."
msgstr ""

#: gettingstarted.html:816
msgid "Minimizes current window."
msgstr ""

#: gettingstarted.html:817
msgid "Maximizes current window."
msgstr ""

#: gettingstarted.html:818
msgid "Space"
msgstr ""

#: gettingstarted.html:818
msgid ""
"Displays the window menu with options such as 'Always on Top' and 'Minimize'"
" and 'Maximize' and above commands."
msgstr ""

#: gettingstarted.html819, 820, 821, 822, 823, 824, 825
msgid "Numpad"
msgstr ""

#: gettingstarted.html:819
msgid "Place window in top left corner of screen."
msgstr ""

#: gettingstarted.html:820
msgid "Place window in top half of screen."
msgstr ""

#: gettingstarted.html:821
msgid "Place window in top right corner of screen."
msgstr ""

#: gettingstarted.html:822
msgid "Center/Maximize the window in the middle of the screen."
msgstr ""

#: gettingstarted.html:823
msgid "Place window in the bottom left corner of the screen."
msgstr ""

#: gettingstarted.html:824
msgid "Place window in the bottom half of the screen."
msgstr ""

#: gettingstarted.html:825
msgid "Place window in the bottom right corner of the screen."
msgstr ""

#: gettingstarted.html:827
msgid "Compiz Effects Shortcuts"
msgstr ""

#: gettingstarted.html:828
msgid ""
"All of the shortcuts listed in this section require Compiz to be enabled."
msgstr ""

#: gettingstarted.html:830
msgid "Toggles 'Show Desktop'."
msgstr ""

#: gettingstarted.html:832
msgid ""
"Enables an 'expose' like feature that presents you with all the windows you "
"currently have open, allowing you to select the one you wish to give focus "
"to."
msgstr ""

#: gettingstarted.html:833
msgid ""
"Unfolds your workspace cube allowing you to see more than one of your "
"workspaces at once, using the left and right cursor keys with this active "
"will allow you to select the workspace you wish to use."
msgstr ""

#: gettingstarted.html835, 836, 837, 838, 839, 840, 841
msgid "Super"
msgstr ""

#: gettingstarted.html:835
msgid "Zoom out, show workspace switcher."
msgstr ""

#: gettingstarted.html:836
msgid ""
"Enables the 'scale' effect, it shows all windows from the current workspace."
msgstr ""

#: gettingstarted.html:837
msgid "Enables the 'scale' effect, it shows all windows from all workspaces."
msgstr ""

#: gettingstarted.html:838
msgid "Invert colors of the focused window."
msgstr ""

#: gettingstarted.html:839
msgid "Invert colors for the whole screen."
msgstr ""

#: gettingstarted.html:840
msgid "Mouse Scroll"
msgstr ""

#: gettingstarted.html:840
msgid "Zooms in on the screen."
msgstr ""

#: gettingstarted.html841, 843
msgid "Middle Mouse Button"
msgstr ""

#: gettingstarted.html:841
msgid "Select a region to zoom into, using a rectangle."
msgstr ""

#: gettingstarted.html:842
msgid "Left Mouse Button"
msgstr ""

#: gettingstarted.html:842
msgid "Move focused window."
msgstr ""

#: gettingstarted.html:843
msgid "Resize focused window."
msgstr ""

#: gettingstarted.html:844
msgid "Right Mouse Button"
msgstr ""

#: gettingstarted.html:844
msgid "Show window menu."
msgstr ""

#: gettingstarted.html:861
msgid ""
"Your machine is made up of various components that interact with one "
"another. This information is useful when providing support to ensure "
"hardware and software works on your machine and Ubuntu MATE."
msgstr ""

#: gettingstarted.html:867
msgid "Please wait while the information is being populated..."
msgstr ""

#: gettingstarted.html:869
msgid "Basic"
msgstr ""

#: gettingstarted.html:870
msgid "Detailed"
msgstr ""

#: gettingstarted.html:871
msgid "Utilities"
msgstr ""

#: gettingstarted.html:882
msgid "Distribution:"
msgstr ""

#: gettingstarted.html:883
msgid "Kernel:"
msgstr ""

#: gettingstarted.html:884
msgid "Motherboard:"
msgstr ""

#: gettingstarted.html:885
msgid "Boot Mode:"
msgstr ""

#: gettingstarted.html:889
msgid "Processor"
msgstr ""

#: gettingstarted.html:891
msgid "Model:"
msgstr ""

#: gettingstarted.html:892
msgid "Speed:"
msgstr ""

#: gettingstarted.html:893
msgid "Architecture in use:"
msgstr ""

#: gettingstarted.html:894
msgid "Architecture(s) supported:"
msgstr ""

#: gettingstarted.html:898
msgid "Components"
msgstr ""

#: gettingstarted.html:900
msgid "Memory (RAM):"
msgstr ""

#: gettingstarted.html:901
msgid "Graphics:"
msgstr ""

#: gettingstarted.html:902
msgid "Internet Access:"
msgstr ""

#: gettingstarted.html:903
msgid "Connected"
msgstr ""

#: gettingstarted.html:904
msgid "Not Connected"
msgstr ""

#: gettingstarted.html:908
msgid "Storage"
msgstr ""

#: gettingstarted.html:910
msgid "Root File System:"
msgstr ""

#: gettingstarted.html:911
msgid "Entire Disk Capacity:"
msgstr ""

#: gettingstarted.html:912
msgid "Allocated:"
msgstr ""

#: gettingstarted.html:913
msgid "Free Space:"
msgstr ""

#: gettingstarted.html:915
msgid "Why do I see two units?"
msgstr ""

#: gettingstarted.html:919
msgid "SI or IEC Units?"
msgstr ""

#: gettingstarted.html:921
msgid "Decimal (SI Standard)"
msgstr ""

#: gettingstarted.html:923
msgid "1 KB = 1000 bytes"
msgstr ""

#: gettingstarted.html:924
msgid "Pronounced as \"ga\" - like \"Mega\", \"Giga\"."
msgstr ""

#: gettingstarted.html:925
msgid "Common in every day applications and file storage."
msgstr ""

#: gettingstarted.html:929
msgid "Binary (IEC Standard)"
msgstr ""

#: gettingstarted.html:931
msgid "1 KiB = 1024 bytes"
msgstr ""

#: gettingstarted.html:932
msgid "Pronounced as \"bi\" - like \"Mebi\", \"Gibi\"."
msgstr ""

#: gettingstarted.html:933
msgid "Common in RAM, drives and partitions, including tools like GParted."
msgstr ""

#: gettingstarted.html:942
msgid "The following information is retrieved directly by the"
msgstr ""

#: gettingstarted.html:942
msgid "command line program."
msgstr ""

#: gettingstarted.html:943
msgid "Could not gather data."
msgstr ""

#: gettingstarted.html:955
msgid "System Profiler and Benchmark"
msgstr ""

#: gettingstarted.html:956
msgid ""
"See more specific system information in a graphical window, including "
"connected USBs, PCI devices and a simple CPU benchmark."
msgstr ""

#: gettingstarted.html:971
msgid "Disks"
msgstr ""

#: gettingstarted.html:972
msgid ""
"A pre-installed utility that allows basic configuration to your disks and "
"partitions, including formatting, partitioning, mounting, viewing S.M.A.R.T "
"data and writing images to SD cards/USB drives."
msgstr ""

#: gettingstarted.html:989
msgid ""
"An advanced tool for viewing, creating, modifying and deleting partitions. "
"It also has features to expand/shrink file systems and supports a wide range"
" of formats."
msgstr ""

#: gettingstarted.html:1004
msgid "System Monitor"
msgstr ""

#: gettingstarted.html:1005
msgid ""
"Views currently running processes, resource usage, and the amount of space "
"used and free for currently mounted file systems."
msgstr ""

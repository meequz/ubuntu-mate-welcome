#
# , 2016.
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-03-18 21:06+0300\n"
"Last-Translator: \n"
"Language-Team: American English <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Report-Msgid-Bugs-To: you@example.com\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<"
"=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Language: be_BY\n"
"X-Source-Language: C\n"
"Language: en_US\n"
"X-Generator: Lokalize 1.5\n"

#: rpi.html:17
msgid "Raspberry Pi Information"
msgstr "Інфармацыя пра Raspberry Pi"

#: rpi.html:25
msgid "A few important notes."
msgstr "Некалькі важных заўваг."

#: rpi.html:26
msgid ""
"While the experience is visually similar to the desktop edition of Ubuntu "
"MATE, the underlying architecture is very different. This page will "
"highlight a few of these differences."
msgstr ""
"Хоць карыстанне знешне і падобнае да дэсктопнай рэдакцыі Ubuntu MATE, "
"унутраная будова сістэмы моцна адрозніваецца. Гэтая старонка распавядае пра "
"некалькі такіх адрозненняў."

#: rpi.html:32
msgid "Resizing the file system"
msgstr ""

#: rpi.html:35
msgid "Your file system has already been resized."
msgstr ""

#: rpi.html:38
msgid ""
"After setting up Ubuntu MATE for the first time on your Raspberry Pi, the "
"file system will be restricted to the size of the original image, which is "
"about 3.9 GB. Use this button to resize and fill the entire Micro-SD card."
msgstr ""

#: rpi.html:42
msgid "Resize Now"
msgstr ""

#: rpi.html:46
msgid "Incompatible Software"
msgstr ""

#: rpi.html:47
msgid ""
"Some packages available for Ubuntu have only been written and compiled for "
"the"
msgstr ""

#: rpi.html48, 103
msgid "or"
msgstr ""

#: rpi.html:49
msgid "architecture. As the Raspberry Pi is based on"
msgstr ""

#: rpi.html:50
msgid ""
"these packages will not be available to install. Depending on the package, "
"it may be possible to compile these yourself or use an existing port."
msgstr ""

#: rpi.html:57
msgid "Incompatible:"
msgstr ""

#: rpi.html:79
msgid "Upgrades"
msgstr ""

#: rpi.html:80
msgid "Please"
msgstr ""

#: rpi.html:80
msgid "do not"
msgstr ""

#: rpi.html:80
msgid "attempt to"
msgstr ""

#: rpi.html:80
msgid "upgrade"
msgstr ""

#: rpi.html:81
msgid ""
"your Raspberry Pi to a newer version of the distribution (for instance, from "
"15.04 to 15.10), as the underlying kernel is"
msgstr ""

#: rpi.html:83
msgid ""
"to do this. This process will take a very long time to complete while "
"potentially filling up your SD card to a point where there is no more free "
"space."
msgstr ""

#: rpi.html:88
msgid ""
"It is safer to back up all of your data you wish to keep and re-flash the "
"card with the new image. Attempting to upgrade may corrupt the SD card, "
"prevent your installation from booting, or cause severe glitches."
msgstr ""

#: rpi.html:92
msgid "You can, however, install"
msgstr ""

#: rpi.html:92
msgid "regular updates"
msgstr ""

#: rpi.html:92
msgid "via the"
msgstr ""

#: rpi.html94, 103
msgid "Software Updater"
msgstr ""

#: rpi.html:94
msgid "utility for your installed software."
msgstr ""

#: rpi.html:97
msgid "Kernel Updates"
msgstr ""

#: rpi.html:98
msgid "The same kernel provided by the"
msgstr ""

#: rpi.html:99
msgid "Raspberry Pi foundation"
msgstr ""

#: rpi.html:100
msgid ""
"is used in this edition of Ubuntu MATE. As this kernel is delivered like a "
"\"firmware\" blob, updates are not distributed via the"
msgstr ""

#: rpi.html:105
msgid "Instead, to update the kernel, open a terminal and run:"
msgstr ""

#: rpi.html:109
msgid "Hardware Acceleration"
msgstr ""

#: rpi.html:110
msgid "Currently, hardware accelerated applications are"
msgstr ""

#: rpi.html:110
msgid "not supported"
msgstr ""

#: rpi.html:111
msgid "unlike"
msgstr ""

#: rpi.html:112
msgid ""
"Applications that depend on OpenGL ES libraries or require the GPU will fail "
"to start."
msgstr ""

#: rpi.html:115
msgid "For playing videos, the application"
msgstr ""

#: rpi.html:115
msgid ""
"will be able to do this and is pre-installed. If you are looking to play "
"MPEG-2 or VC-1 video files then"
msgstr ""

#: rpi.html:117
msgid "you will need MPEG-2 and/or VC-1 licenses from the"
msgstr ""

#: rpi.html:118
msgid "Raspberry Pi Store."
msgstr ""

#: rpi.html:121
msgid "Enable/Disable X11"
msgstr ""

#: rpi.html:122
msgid "For users who are looking to create their own headless"
msgstr ""

#: rpi.html:122
msgid "\"server\""
msgstr ""

#: rpi.html:122
msgid ""
"using Ubuntu MATE, there is a utility for toggling the graphical environment."
msgstr ""

#: rpi.html:125
msgid "To disable X11 and login via the console:"
msgstr ""

#: rpi.html:129
msgid "To enable X11 to restore the Ubuntu MATE desktop:"
msgstr ""

#: rpi.html:132
msgid "Changes take effect after a reboot."
msgstr ""

#: rpi.html:136
msgid "Release Notes"
msgstr "Нататкі пра выпуск"

#: rpi.html:137
msgid ""
"To read up on release notes and changes by version, visit the Ubuntu MATE "
"website."
msgstr ""

#: rpi.html:144
msgid "Raspberry Pi is a trademark of the"
msgstr ""

